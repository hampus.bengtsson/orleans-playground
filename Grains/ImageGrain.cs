using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Orleans;
using Orleans.Concurrency;

namespace OrleansBasics
{
    public class ImageGrain : Grain, IImage
    {
        private readonly IBlobCacheService blobCacheService;

        public ImageGrain(IBlobCacheService blobCacheService)
        {
            this.blobCacheService = blobCacheService;
        }

        public async Task<Immutable<byte[]>> GetThumbnail()
        {
            var path = this.GetPrimaryKeyString();

            if (blobCacheService.TryLoad(path, out var cachedThumbnail))
            {
                Console.WriteLine("  CACHE HIT!");
                return cachedThumbnail;
            }

            var timer = Stopwatch.StartNew();
            using var inputFileStream = File.OpenRead(path);
            using var inputMemoryStream = new MemoryStream();
            await inputFileStream.CopyToAsync(inputMemoryStream);
            var loadingMillis = timer.ElapsedMilliseconds;

            var thumbnailBytes = await GrainFactory.GetGrain<IImageResizer>(0).MakeThumbnail(inputMemoryStream.ToArray().AsImmutable());

            blobCacheService.Store(path, thumbnailBytes);
            timer.Stop();

            var resizeMillis = timer.ElapsedMilliseconds - loadingMillis;
            Console.WriteLine($"  Processed image in {timer.ElapsedMilliseconds} ms (loading in {loadingMillis} ms, resizing in {resizeMillis} ms).");

            return thumbnailBytes;
        }
    }
}
