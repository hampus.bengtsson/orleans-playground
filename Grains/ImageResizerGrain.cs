using System.IO;
using System.Threading.Tasks;
using Orleans;
using Orleans.Concurrency;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace OrleansBasics
{
    [StatelessWorker]
    public class ImageResizerGrain : Grain, IImageResizer
    {
        public Task<Immutable<byte[]>> MakeThumbnail(Immutable<byte[]> image)
        {
            using var loadedImage = Image.Load(image.Value);
            loadedImage.Mutate(x => x
                .Resize(new ResizeOptions
                {
                    Mode = ResizeMode.Crop,
                    Size = new SixLabors.Primitives.Size(50, 50),
                })
            );

            using var outputMemoryStream = new MemoryStream();
            loadedImage.SaveAsPng(outputMemoryStream);
            var thumbnailBytes = outputMemoryStream.ToArray();
            return Task.FromResult(thumbnailBytes.AsImmutable());
        }
    }
}
