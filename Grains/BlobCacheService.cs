﻿using Microsoft.Extensions.Caching.Memory;
using Orleans.Concurrency;

namespace OrleansBasics
{
    public class BlobCacheService : IBlobCacheService
    {
        private readonly MemoryCache cache;

        public BlobCacheService(long bytesLimit)
        {
            cache = new MemoryCache(new MemoryCacheOptions { SizeLimit = bytesLimit });
        }

        public void Store(string key, Immutable<byte[]> blob) =>
            cache.Set(key, blob, new MemoryCacheEntryOptions { Size = blob.Value.Length });

        public bool TryLoad(string key, out Immutable<byte[]> blob) => 
            cache.TryGetValue(key, out blob);
    }
}
