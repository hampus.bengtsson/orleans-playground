using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Concurrency;

namespace OrleansBasics
{
    public class HelloGrain : Grain, IHello
    {
        private readonly ILogger logger;
        private int state;

        public HelloGrain(ILogger<HelloGrain> logger)
        {
            this.logger = logger;
        }

        [ReadOnly]
        public Task<string> Read()
        {
            //await Task.Delay(5_000);
            return Task.FromResult(state.ToString());
        }

        public Task Write(int state)
        {
            this.state = state;
            return Task.CompletedTask;
        }

        Task<string> IHello.SayHello(string command)
        {
            logger.LogInformation($"\n SayHello message received: greeting = '{command}'");
            return Task.FromResult($"\n Client said: '{command}', so HelloGrain says: Hello!");
        }
    }
}
