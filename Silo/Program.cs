﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;

namespace OrleansBasics
{
    public static class Program
    {
        public static async Task<int> Main()
        {
            try
            {
                var host = await StartSilo();
                Console.WriteLine("\n\n Press Enter to terminate...\n\n");
                Console.ReadLine();

                await host.StopAsync();

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return 1;
            }
        }

        private static async Task<ISiloHost> StartSilo()
        {
            // define the cluster configuration
            var builder = new SiloHostBuilder()
                .UseAdoNetClustering(options =>
                {
                    //options.ConnectionString = @"Server=(localdb)\MSSQLLocalDB;Integrated Security=true";
                    options.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=OrleansTest;Integrated Security=True;Pooling=False;Max Pool Size=200;MultipleActiveResultSets=True";
                    options.Invariant = "System.Data.SqlClient";
                })
                .Configure((Action<ClusterOptions>)(options =>
                {
                    options.ClusterId = "dev";
                    options.ServiceId = "OrleansBasics";
                }))
                .ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(HelloGrain).Assembly).WithReferences())
                .ConfigureEndpoints(
                    hostname: "localhost",
                    siloPort: Utils.FindFreePort(),
                    gatewayPort: Utils.FindFreePort()
                )
                .ConfigureServices(services =>
                {
                    services.AddSingleton<IBlobCacheService, BlobCacheService>(_ => new BlobCacheService(bytesLimit: 15_000));
                })
                .UseDashboard(options => { })
                .ConfigureLogging(logging => logging.SetMinimumLevel(LogLevel.Error).AddConsole());

            var host = builder.Build();
            await host.StartAsync();
            return host;
        }
    }
}
