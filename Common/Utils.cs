﻿using System.Net;
using System.Net.Sockets;

namespace OrleansBasics
{
	public static class Utils
	{
		public static int FindFreePort()
		{
			int port = 0;
			Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			try
			{
				var localEP = new IPEndPoint(IPAddress.Any, 0);
				socket.Bind(localEP);
				localEP = (IPEndPoint)socket.LocalEndPoint;
				port = localEP.Port;
			}
			finally
			{
				socket.Close();
			}
			return port;
		}
	}
}
