﻿using Orleans.Concurrency;

namespace OrleansBasics
{
    public interface IBlobCacheService
    {
        void Store(string key, Immutable<byte[]> blob);
        bool TryLoad(string key, out Immutable<byte[]> blob);
    }
}
