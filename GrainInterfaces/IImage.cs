using System.Threading.Tasks;
using Orleans.Concurrency;

namespace OrleansBasics
{
    public interface IImage : Orleans.IGrainWithStringKey
    {
        Task<Immutable<byte[]>> GetThumbnail();
    }
}
