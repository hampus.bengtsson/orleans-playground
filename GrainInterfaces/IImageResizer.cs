using System.Threading.Tasks;
using Orleans.Concurrency;

namespace OrleansBasics
{
    public interface IImageResizer : Orleans.IGrainWithIntegerKey
    {
        Task<Immutable<byte[]>> MakeThumbnail(Immutable<byte[]> image);
    }
}
