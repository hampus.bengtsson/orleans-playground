﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;

namespace OrleansBasics
{
    public class Program
    {
        static async Task Main()
        {
            Console.Write("Connecting...");
            while (true)
            {
                try
                {
                    using var client = await ConnectClient();
                    while (true)
                    {
                        var foo = Console.ReadLine();
                        //if (foo.Key == ConsoleKey.Escape)
                        //{
                        //	break;
                        //}
                        _ = DoClientWork(client, rawCommand: foo);
                    }

                    //return 0;
                }
                catch (Exception)
                {
                    //Console.WriteLine($"\nException while trying to run client: {e.Message}");
                    //Console.WriteLine("Make sure the silo the client is trying to connect to is running.");
                    //Console.WriteLine("\nPress any key to exit.");
                    //Console.ReadKey();
                    //return 1;
                    Console.Write(".");
                    await Task.Delay(millisecondsDelay: 1000);
                }
            }
        }

        private static async Task<IClusterClient> ConnectClient()
        {
            var client = new ClientBuilder()
                .UseAdoNetClustering((AdoNetClusteringClientOptions options) =>
                {
                    //options.ConnectionString = @"Server=(localdb)\MSSQLLocalDB;Integrated Security=true";
                    options.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=OrleansTest;Integrated Security=True;Pooling=False;Max Pool Size=200;MultipleActiveResultSets=True";
                    options.Invariant = "System.Data.SqlClient";
                })
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = "dev";
                    options.ServiceId = "OrleansBasics";
                })
                .ConfigureLogging(logging => logging.SetMinimumLevel(LogLevel.Critical).AddConsole())
                .Build();

            await client.Connect();
            Console.WriteLine(" Client successfully connected to silo host \n");
            return client;
        }

        private static async Task DoClientWork(IClusterClient client, string rawCommand)
        {
            var friend = client.GetGrain<IHello>(0);

            if (int.TryParse(rawCommand, out var number))
            {
                await friend.Write(number);
                Console.WriteLine($"Setting state to {number}\n");
                return;
            }

            if (rawCommand.StartsWith("image "))
            {
                var path = rawCommand.Remove(0, "image ".Length).Trim();
                var imageGrain = client.GetGrain<IImage>(path);
                var image = (await imageGrain.GetThumbnail()).Value;

                Console.WriteLine($"Got image of {image.Length} bytes\n");
                return;
            }

            var read = await friend.Read();
            Console.WriteLine($"   Read: {read}\n");
        }
    }
}
